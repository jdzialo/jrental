<div class="container-fluid">
    <!--<div class="hidden-xs row box_logo">
        <div class="col-xs-12">
            <img src="/themes/flatly/img/logo_jeunesse_gray.png">
        </div>
    </div>-->
    
    <div class="row box_slider">
        @if (isset($posts))
        @foreach($posts as $post)
        <div class="col-xs-12 col-sm-4 col-md-2">
            <div class="box_slide">
                <div class="foto_box">
                    @if($post->files()->where('zone', 'thumbnail')->get()->first())
                        <img class="foto" src="{{ $post->files()->where('zone', 'thumbnail')->get()->first()->path }}" >
                    @else
                        @if($post->getThumbnailAttribute() != '')
                        <img class="foto" src="{{ $post->getThumbnailAttribute()->getThumbnail('blogThumb') }}" >
                        @endif
                    @endif

                    <img class="rect_round" src="/themes/flatly/img/icons/rect_round.png" >
                    <!--<div class="label_star"><i class="fa fa-star" aria-hidden="true"></i></div>-->
                </div>
                <div class="content_box">
                    <div class="data">{{ $post->created_at->format('d-m-Y') }}</div>
                    <div class="title">{{ $post->title }}</div>
                    <a href="{{ URL::route($currentLocale . '.blog.slug', [$post->slug]) }}" class="btn_gray">Przeczytaj cały wpis</a>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>



@push('style')
{!! Theme::style('vendor/slick-carousel/slick/slick.css') !!}
@endpush
@push('scripts')
{!! Theme::script('vendor/slick-carousel/slick/slick.min.js') !!}
<script>
    $(document).ready(function () {
        $('.box_slider').slick({
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            dots: false,
            speed: 300,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            prevArrow: '<div class="prevArrow"><div class="ico_arrow_prev"></div></div>',
            nextArrow: '<div class="nextArrow"><div class="ico_arrow_next"></div></div>',
            responsive: [
                {
                    breakpoint: 1301,
                    settings: {slidesToShow: 4}
                },
                {
                    breakpoint: 993,
                    settings: {slidesToShow: 3}
                },
                {
                    breakpoint: 769,
                    settings: {slidesToShow: 2, centerMode: true, centerPadding: '0px'}
                },
                {
                    breakpoint: 480,
                    settings: {slidesToShow: 1, centerMode: true, centerPadding: '60px'}
                },
                {
                    breakpoint: 400,
                    settings: {slidesToShow: 1, centerMode: true, centerPadding: '30px'}
                },
                {
                    breakpoint: 350,
                    settings: {slidesToShow: 1, centerMode: true, centerPadding: '0px'}
                },
            ]
        });
    });
</script>
@endpush
<footer class="container-fluid">
    <div class="container">
        <div class="row rtop">
            <div class="col-sm-3 col_l">
                <img src="/themes/flatly/img/logo_jrental_bottom.png">
                <img src="/themes/flatly/img/logo_jeunesse_bottom.png" class="logo_jeunesse_bottom">
            </div>
            <div class="hidden-xs col-sm-9 col_r">
                @menu('Menu')
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="separator_blue"></div>
            </div>
        </div>
        <div class="row rbottom">
            <div class="col-sm-6 col_l">
                Copyright &COPY; 2016. Wszystkie prawa zastrzeżone <a href="http://jrental.pl">jrental.pl</a>
            </div>
            <div class="col-sm-6 col_r">
                Projekt i realizacja <a href="http://www.moonbite.pl" class="strony">strony internetowej</a>: <a href="http://www.moonbite.pl">moonbite.pl</a>
            </div>
        </div>
    </div>
</footer>

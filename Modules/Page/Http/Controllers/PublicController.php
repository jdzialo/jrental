<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Dashboard\Services\StatisticsService;
use Modules\Page\Http\Requests\CreateContactFormRequest;
use Illuminate\Session;
use Modules\Slider\Entities\Slider;

class PublicController extends BasePublicController {

    /**
     * @var PageRepository
     */
    private $page;
    
       /**
     * @var SliderRepository
     */
    private $slider;

    /**
     * @var Application
     */
    private $app;
    public $email;

    public function __construct(PageRepository $page, Application $app, \Modules\Blog\Repositories\PostRepository $post , SliderRepository $slider) {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
        $this->post = $post;
        $this->slider = $slider;
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug) {
        
        $page = $this->findPageForSlug($slug);

        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        return view($template, compact('page'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage() {
        
        $posts = $this->post->allTranslatedIn($this->app->getLocale())->take(7);
        $page = $this->page->findHomepage();

        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        /**
         * Executing statiscits
         */
        if (!isset($_COOKIE['Vstatistics'])) {
            StatisticsService::stat($_SERVER['REMOTE_ADDR']); //  //'46.174.213.44'
        }
        if (empty($posts)) {
            return view($template, compact('page'));
        }
        
        return view($template, compact('page', 'posts'));
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug) {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page) {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page) {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }

    public function contactpage() {     
        try
        {
           $slider = $this->slider->findByTitleInLocale("contact" , $this->app->getLocale());  
           return view('contact.index', compact('slider'));
        }
         
         catch (\Exception $e) {
              return redirect()->route("homepage")
                        ->withErrors("Wystąpił błąd"+$e->getMessage());
         }
    }
        
    

    public function storecontact(CreateContactFormRequest $request) {

        $reqestAll = $request->all();
        $this->email = $reqestAll['email'];

        \Mail::send('contact.email', $reqestAll, function($message) {
            $message->from($this->email);
            $message->to(env('MAIL_USERNAME'), 'Admin')->subject(trans('page::contact.mail.subject'));
        });

        return \Redirect::route('page', '/')
                        ->with('message', trans('page::contact.contact.send contactform'));
    }

    public function gallery() {
        if ($this->gallery->allTranslatedIn(App::getLocale())->first() != null) {
            $galleries = $this->gallery->allTranslatedIn(App::getLocale())->first()->files()->get();
            return view('gallery.index', compact('galleries'));
        }
        return view('gallery.index');
    }

    public function slider() {
        if (Slider::with(['slides', 'slides.translations', 'slides.file'])->first() != null) {
            $slides = Slider::with(['slides', 'slides.translations', 'slides.file'])->first()->files()->get();
        }
        
        return $slides;
    }
    
 

}

<div class="row">
    <div class="col-xs-12 bluecontainerslim">
        <div class="editarea">
            {{ trans('page::contact.contact.receive message') }}
        </div>
    </div>
</div>
<div class="box-body">
    <div class="row">
        <div class="col-sm-6">
            <div class='form-group{{ $errors->has("firstname") ? ' has-error' : '' }}'>
                {!! Form::text("firstname", old("firstname"), ['onfocus' => 'this.placeholder="trans("page::contact.form.firstname")"', 'onblur' => 'this.placeholder="trans("page::contact.form.firstname")"', 'placeholder' => trans('page::contact.form.firstname')]) !!}
                {!! $errors->first("firstname", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class='form-group{{ $errors->has("lastname") ? ' has-error' : '' }}'>
                {!! Form::text("lastname", old("lastname"), ['onfocus' => 'this.placeholder="trans("page::contact.form.lastname")"', 'onblur' => 'this.placeholder="trans("page::contact.form.lastname")"', 'placeholder' => trans('page::contact.form.lastname')]) !!}
                {!! $errors->first("lastname", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class='form-group{{ $errors->has("phone") ? ' has-error' : '' }}'>      
                {!! Form::text("phone", old("phone"), ['onfocus' => 'this.placeholder="trans("page::contact.form.phone")"', 'onblur' => 'this.placeholder="trans("page::contact.form.phone")"',  'placeholder' => trans('page::contact.form.phone')]) !!}
                {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
                {!! Form::email("email", old("email"), ['onfocus' => 'this.placeholder="trans("page::contact.form.email")"', 'onblur' => 'this.placeholder="trans("page::contact.form.email")"',  'placeholder' => trans('page::contact.form.email')]) !!}
                {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class='form-group{{ $errors->has("address") ? ' has-error' : '' }}'>      
                {!! Form::text("address", old("address"), ['onfocus' => 'this.placeholder="trans("page::contact.form.addess")"', 'onblur' => 'this.placeholder="trans("page::contact.form.address")"',  'placeholder' => trans('page::contact.form.address')]) !!}
                {!! $errors->first("address", '<span class="help-block">:message</span>') !!}
            </div>
        </div> 
        <div class="col-sm-6">
            <div class='form-group{{ $errors->has("address2") ? ' has-error' : '' }}'>      
                {!! Form::text("address2", old("address2"), ['onfocus' => 'this.placeholder="trans("page::contact.form.address2")"', 'onblur' => 'this.placeholder="trans("page::contact.form.address2")"',  'placeholder' => trans('page::contact.form.address2')]) !!}
                {!! $errors->first("address2", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-3">
            <div class='form-group{{ $errors->has("city") ? ' has-error' : '' }}'>      
                {!! Form::text("city", old("city"), ['onfocus' => 'this.placeholder="trans("page::contact.form.city")"', 'onblur' => 'this.placeholder="trans("page::contact.form.city")"',  'placeholder' => trans('page::contact.form.city')]) !!}
                {!! $errors->first("city", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class='form-group{{ $errors->has("county") ? ' has-error' : '' }}'>      
                {!! Form::text("county", old("county"), ['onfocus' => 'this.placeholder="trans("page::contact.form.county")"', 'onblur' => 'this.placeholder="trans("page::contact.form.county")"',  'placeholder' => trans('page::contact.form.county')]) !!}
                {!! $errors->first("county", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class='form-group{{ $errors->has("postalcode") ? ' has-error' : '' }}'>      
                {!! Form::text("postalcode", old("postalcode"), ['onfocus' => 'this.placeholder="trans("page::contact.form.postalcode")"', 'onblur' => 'this.placeholder="trans("page::contact.form.postalcode")"',  'placeholder' => trans('page::contact.form.postalcode')]) !!}
                {!! $errors->first("postalcode", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class='form-group{{ $errors->has("country") ? ' has-error' : '' }}'>      
                {!! Form::text("country", old("country"), ['onfocus' => 'this.placeholder="trans("page::contact.form.country")"', 'onblur' => 'this.placeholder="trans("page::contact.form.country")"',  'placeholder' => trans('page::contact.form.country')]) !!}
                {!! $errors->first("country", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <div class='form-group{{ $errors->has("contactmessage") ? ' has-error' : '' }}'>
                {!! Form::textarea("contactmessage", old("contactmessage"), ['onfocus' => 'this.placeholder="trans("page::contact.form.message")"', 'onblur' => 'this.placeholder="trans("page::contact.form.message")"',  'placeholder' => trans('page::contact.form.message')]) !!}
                {!! $errors->first("contactmessage", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                {!! Form::submit(trans('page::contact.button.send'), 
                array('class' => 'btn_blue')) !!}
            </div>
        </div>
    </div>
    <div>
        <div class="claus text">{{ trans('page::contact.contact.agree') }}</div>
    </div>
</div>
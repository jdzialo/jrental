<?php

return [
    'title'  => 'Menu',
    'titles' => [
        'menu' => 'Menu management',
        'create menu' => 'Create a menu',
        'edit menu' => 'Edit a menu',
        'create menu item' => 'Create a menu item',
        'edit menu item' => 'Edit a menu item',
    ],
    'breadcrumb' => [
        'menu' => 'Zarzadzanie menu',
        'create menu' => 'Utwórz menu',
        'edit menu' => 'Edytuj menu',
        'create menu item' => 'Utwórz element menu',
        'edit menu item' => 'Edytuj element menu',
    ],
    'button' => [
        'create menu item' => 'Utwórz element menu',
        'create menu' => 'Utwórz menu',
    ],
    'table' => [
        'name' => 'Nazwa',
        'title' => 'Tytuł',
    ],
    'form' => [
        'title' => 'Tytuł',
        'name' => 'Nazwa',
        'status' => 'Aktywny',
        'uri' => 'URI',
        'url' => 'URL',
        'primary' => 'Primary menu (used for front-end routing)',
    ],
    'navigation' => [
        'back to index' => 'Go back to the menu index',
    ],
    'list resource' => 'List menus',
    'create resource' => 'Create menus',
    'edit resource' => 'Edit menus',
    'destroy resource' => 'Delete menus',
];

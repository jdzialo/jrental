<?php

namespace Modules\Category\Events\Handlers;

use Modules\Category\Events\CategoryWasCreated;
use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Setting\Contracts\Setting;

class RootCategoryItemCreator
{
    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;
    /**
     * @var Setting
     */
    private $setting;

    public function __construct(CategoryItemRepository $categoryItem, Setting $setting)
    {
        $this->categoryItem = $categoryItem;
        $this->setting = $setting;
    }

    public function handle(CategoryWasCreated $event)
    {
        $data = [
            'category_id' => $event->category->id,
            'position' => 0,
            'is_root' => true,
        ];

        foreach ($this->getEnabledLocales() as $locale) {
            $data[$locale]['title'] = 'root';
        }

        $this->categoryItem->create($data);
    }

    /**
     * Return an array of enabled locales
     * @return array
     */
    private function getEnabledLocales()
    {
        return json_decode($this->setting->get('core::locales', '{"en"}'));
    }
}

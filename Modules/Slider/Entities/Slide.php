<?php

namespace Modules\Slider\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
//use Modules\Media\Entities\File;

class Slide extends Model {

    use Translatable;

    protected $table = 'slider__slides';
    public $translatedAttributes = ['title', 'content', 'url'];
    protected $fillable = [
        'slider_id',
        'file_id'
    ];

    public function file() {
        return $this->belongsTo('Modules\Media\Entities\File');
    }

}

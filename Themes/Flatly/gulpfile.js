var gulp = require("gulp");
var shell = require('gulp-shell');
var elixir = require('laravel-elixir');
var themeInfo = require('./theme.json');
elixir.config.assetsPath = './resources';
// Extension ---------------------------
var imagemin = require('gulp-imagemin');
var size = require('gulp-size');

gulp.task('img', function () {
    return gulp.src('resources/img/*')
            .pipe(imagemin())
            .pipe(size())
            .pipe(gulp.dest('assets/img'))
});
gulp.task('img_bg', function () {
    return gulp.src('resources/img/bg/*')
            .pipe(imagemin())
            .pipe(size())
            .pipe(gulp.dest('assets/img/bg'))
});
gulp.task('img_icons', function () {
    return gulp.src('resources/img/icons/*')
            .pipe(imagemin())
            .pipe(size())
            .pipe(gulp.dest('assets/img/icons'))
});
gulp.task('img_tmp', function () {
    return gulp.src('resources/img/tmp/*')
            .pipe(imagemin())
            .pipe(size())
            .pipe(gulp.dest('assets/img/tmp'))
});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.task('img');
    mix.task('img_bg');
    mix.task('img_icons');
    mix.task('img_tmp');
    /**
     * Compile less
     */
    mix.less([
        "main.less"
    ], 'assets/css/main.css')
    .copy(
        'assets',
        '../../public/themes/' + themeInfo.name.toLowerCase()
    );

    /**
     * Concat scripts
     */
    mix.scripts([
        '/vendor/jquery/dist/jquery.js',
        '/vendor/bootstrap/dist/js/bootstrap.min.js',
        '/vendor/prism/prism.js',
        '/js/bootswatch.js'
    ], null, 'resources')
	.copy('resources/vendor', '../../public/themes/' + themeInfo.name.toLowerCase() + '/vendor' );

    /**
     * Copy Bootstrap fonts
     */
    // mix.copy(
    //     'resources/vendor/bootstrap/fonts',
    //     'assets/fonts'
    // );

    /**
     * Copy Fontawesome fonts
     */
    // mix.copy(
    //     'resources/vendor/font-awesome/fonts',
    //     'assets/fonts'
    // );
});

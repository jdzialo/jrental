<?php

namespace Modules\User\Database\Seeders;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SentinelGroupSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        $groups = Sentinel::getRoleRepository();

        // Create an Admin group
        $groups->createModel()->create(
            [
                'name' => 'Super Admin',
                'slug' => 'super-admin',
            ]
        );

        // Create an Admin group
        $groups->createModel()->create(
            [
                'name' => 'Admin',
                'slug' => 'admin',
            ]
        );

        // Create an Users group
        $groups->createModel()->create(
            [
                'name' => 'User',
                'slug' => 'user',
            ]
        );

        // Save the permissions
        $group = Sentinel::findRoleBySlug('super-admin');
        $group->permissions = [
              'dashboard.index' => true,
              'dashboard.update' => true,
              'dashboard.reset' => true,
              /* Workbench */
              'workshop.modules.index' => true,
              'workshop.modules.show' => true,
              'workshop.modules.disable' => true,
              'workshop.modules.enable' => true,
              'workshop.modules.update' => true,
              'workshop.themes.index' => true,
              'workshop.themes.show' => true,
              /* Roles */
              'user.roles.index' => true,
              'user.roles.create' => true,
              'user.roles.edit' => true,
              'user.roles.destroy' => true,
              /* Users */
              'user.users.index' => true,
              'user.users.create' => true,
              'user.users.edit' => true,
              'user.users.destroy' => true,
              /* API keys */
              'account.api-keys.index' => true,
              'account.api-keys.create' => true,
              'account.api-keys.destroy' => true,
              /* Menu */
              'menu.menus.index' => true,
              'menu.menus.create' => true,
              'menu.menus.edit' => true,
              'menu.menus.destroy' => true,
              'menu.menuitems.index' => true,
              'menu.menuitems.create' => true,
              'menu.menuitems.edit' => true,
              'menu.menuitems.destroy' => true,
              /* Media */
              'media.medias.index' => true,
              'media.medias.create' => true,
              'media.medias.edit' => true,
              'media.medias.destroy' => true,
              /* Settings */
              'setting.settings.index' => true,
              'setting.settings.edit' => true,
              /* Page */
              'page.pages.index' => true,
              'page.pages.create' => true,
              'page.pages.edit' => true,
              'page.pages.destroy' => true,
              /* Blog */
              'blog.posts.index' => true,
              'blog.posts.create' => true,
              'blog.posts.edit' => true,
              'blog.posts.destroy' => true,
              'blog.categories.index' => true,
              'blog.categories.create' => true,
              'blog.categories.edit' => true,
              'blog.categories.destroy' => true,
              'blog.tags.index' => true,
              'blog.tags.create' => true,
              'blog.tags.edit' => true,
              'blog.tags.destroy' => true,
              /* Translation */
              'translation.translations.index' => true,
              'translation.translations.edit' => true,
              'translation.translations.export' => true,
              'translation.translations.import' => true,
              /* Tags */
              'tag.tags.index' => true,
              'tag.tags.create' => true,
              'tag.tags.edit' => true,
              'tag.tags.destroy' => true,
          ];
        $group->save();

        // Save the permissions
        $group = Sentinel::findRoleBySlug('admin');
        $group->permissions = [
            'dashboard.index' => true,
            'dashboard.update' => true,
            'dashboard.reset' => true,
            /* Workbench */
            // 'workshop.modules.index' => true,
            // 'workshop.modules.show' => true,
            // 'workshop.modules.disable' => true,
            // 'workshop.modules.enable' => true,
            // 'workshop.modules.update' => true,
            // 'workshop.themes.index' => true,
            // 'workshop.themes.show' => true,
            /* Roles */
            // 'user.roles.index' => true,
            // 'user.roles.create' => true,
            // 'user.roles.edit' => true,
            // 'user.roles.destroy' => true,
            /* Users */
            'user.users.index' => true,
            'user.users.create' => true,
            'user.users.edit' => true,
            'user.users.destroy' => true,
            /* API keys */
            // 'account.api-keys.index' => true,
            // 'account.api-keys.create' => true,
            // 'account.api-keys.destroy' => true,
            /* Menu */
            'menu.menus.index' => true,
            'menu.menus.create' => true,
            'menu.menus.edit' => true,
            'menu.menus.destroy' => true,
            'menu.menuitems.index' => true,
            'menu.menuitems.create' => true,
            'menu.menuitems.edit' => true,
            'menu.menuitems.destroy' => true,
            /* Media */
            'media.medias.index' => true,
            'media.medias.create' => true,
            'media.medias.edit' => true,
            'media.medias.destroy' => true,
            /* Settings */
            'setting.settings.index' => true,
            'setting.settings.edit' => true,
            /* Page */
            'page.pages.index' => true,
            'page.pages.create' => true,
            'page.pages.edit' => true,
            'page.pages.destroy' => true,
            /* Translation */
            'translation.translations.index' => true,
            'translation.translations.edit' => true,
            'translation.translations.export' => true,
            'translation.translations.import' => true,
            /* Tags */
            'tag.tags.index' => true,
            'tag.tags.create' => true,
            'tag.tags.edit' => true,
            'tag.tags.destroy' => true,
        ];
        $group->save();
    }
}

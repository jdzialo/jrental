@extends('layouts.master')

@section('title')
{{ $post->meta_title or $post->title }} | @parent
@stop

@section('meta')
<meta name="title" content="{{ $post->meta_title}}" />
<meta name="description" content="{{ $post->meta_description }}" />
@stop

@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>{{ $post->title }}</h1>
                <span class="date">{{ $post->created_at->format('d-m-Y') }}</span>
                <div class="box_blue_grad2"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_blog_item">
    <div class="row row_content">
        <div class="col-xs-12 col-sm-7 col-md-8">
            <div class="text">
                {!! $post->content !!}
            </div>
            @if($post->getGalleryAttribute() != "")
            <div class="row row_gallery">
                @foreach($post->getGalleryAttribute() as $file)
                <div class="col-sm-4 col_foto">
                    <a href="{{$file->path}}" class="link_foto">
                        <img class="img-responsive foto" src="{{$file->getThumbnail('blogThumb')}}">
                    </a>
                </div>
                @endforeach
            </div>
            @endif
        </div>
        <div class="col-xs-12 col-sm-5 col-md-4">
            @include('partials.offer-ask', ['txt' => "Masz pytania dotyczące CFM ?"])
        </div>
    </div>
</div>
<div style="height: 30px"></div>
@stop

@push('style')
{!! Theme::style('vendor/magnific-popup/dist/magnific-popup.css') !!}
@endpush
@push('scripts')
{!! Theme::script('vendor/magnific-popup/dist/jquery.magnific-popup.min.js') !!}
<script>
    $(document).ready(function () {
        $('.link_foto').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });
</script>
@endpush
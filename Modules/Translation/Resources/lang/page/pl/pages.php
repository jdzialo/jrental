<?php

return [
    'routing' => [
        'gallery' => 'galeria',
        'contact' => 'kontakt',
        'application-for-disable' => 'zastosowanie/dla-niepelnosprawnych',
        'application-for-industry' => 'zastosowanie/w-przemyśle',
        'application-for-architecture' => 'zastosowanie/w-architekturze'
    ],
    'title' => [
        'pages' => 'Strony',
        'create page' => 'Utwórz stronę',
        'edit page' => 'Edytuj stronę',
        'contactpage' => 'Wiadomości kontaktowe'
    ],
    'button' => [
        'create page' => 'Utwórz stronę',
    ],
    'table' => [
        'name' => 'Nazwa',
        'slug' => 'Slug',
    ],
    'form' => [
        'title' => 'Tytuł',
        'slug' => 'Slug',
        'meta_title' => 'Meta tytuł',
        'meta_data' => 'Meta dane',
        'meta_description' => 'Meta opis',
        'og_title' => 'Facebook tytuł',
        'og_description' => 'Facebook opis',
        'og_type' => 'Facebook typ',
        'facebook_data' => 'Dane dla Facebook',
        'template' => 'Szablon strony',
        'is homepage' => 'Strona domowa ?',
        'body' => 'Treść',
    ],
    'validation' => [
        'attributes' => [
            'title' => 'title',
            'body' => 'body',
        ],
    ],
    'facebook-types' => [
        'website' => 'Strona',
        'product' => 'Produkt',
        'article' => 'Artykuł',
    ],
    'navigation' => [
        'back to index' => 'Wróć do indeksu stron',
    ],
];

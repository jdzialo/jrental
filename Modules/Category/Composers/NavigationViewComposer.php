<?php

namespace Modules\Category\Composers;

use Modules\Category\Entities\Categoryitem as CategoryitemEntity;
use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Category\Repositories\CategoryRepository;
use Nwidart\Menus\Builder;
use Nwidart\Menus\Facades\Menu;
use Nwidart\Menus\MenuItem;

class NavigationViewComposer
{
    /**
     * @var CategoryRepository
     */
    private $category;
    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;

    public function __construct(CategoryRepository $category, CategoryItemRepository $categoryItem)
    {
        $this->category = $category;
        $this->categoryItem = $categoryItem;
    }

    public function compose()
    {
        foreach ($this->category->all() as $category) {
            $categoryTree = $this->categoryItem->getTreeForCategory($category->id);
            Category::create($category->name, function (Builder $category) use ($categoryTree) {
                foreach ($categoryTree as $categoryItem) {
                    $this->addItemToCategory($categoryItem, $category);
                }
            });
        }
    }

    /**
     * Add a category item to the category
     * @param CategoryitemEntity $item
     * @param Builder $category
     */
    public function addItemToCategory(CategoryitemEntity $item, Builder $category)
    {
        if ($this->hasChildren($item)) {
            $this->addChildrenToCategory($item->title, $item->items, $category);
        } else {
            $target = $item->uri ?: $item->url;
            $category->url(
                $target,
                $item->title,
                ['target' => $item->target]
            );
        }
    }

    /**
     * Add children to category under the give name
     *
     * @param string $name
     * @param object $children
     * @param Builder|CategoryItem $category
     */
    private function addChildrenToCategory($name, $children, $category)
    {
        $category->dropdown($name, function (CategoryItem $subCategory) use ($children) {
            foreach ($children as $child) {
                $this->addSubItemToCategory($child, $subCategory);
            }
        });
    }

    /**
     * Add children to the given category recursively
     *
     * @param CategoryitemEntity   $child
     * @param CategoryItem $sub
     */
    private function addSubItemToCategory(CategoryitemEntity $child, CategoryItem $sub)
    {
        $sub->url($child->uri, $child->title);

        if ($this->hasChildren($child)) {
            $this->addChildrenToCategory($child->title, $child->items, $sub);
        }
    }

    /**
     * Check if the given category item has children
     *
     * @param  object $item
     * @return bool
     */
    private function hasChildren($item)
    {
        return $item->items->count() > 0;
    }
}

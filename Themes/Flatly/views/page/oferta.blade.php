@extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@endsection
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@endsection

@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>{{ $page->title }}</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_offer">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-8 text">
            {!! $page->body !!}
        </div>
        <div class="col-xs-12 col-sm-5 col-md-4">
            @include('partials.offer-ask', ['txt' => "Masz pytania dotyczące naszej oferty ?"])
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-8 text">
            <p>Najem krótkoterminowy służy zaspokojeniu pojedynczych potrzeb transportowych występujących incydentalnie związanych ze wzmożonym 
                zapotrzebowaniem na tego rodzaju usługi lub występuje w związku z niedyspozycją własnego taboru. Zaś najem średnioterminowy pozwala 
                wykorzystywać  auto dla realizacji zadań firmy w dłuższym okresie np. rok, i ma miejsce zazwyczaj gdy firma nie posiada własnej floty. 
                Jest więc alternatywą pomiędzy dość kosztownym najmem krótkoterminowym a leasingiem, którego minimalny okres trwania wynosi 2 lata.</p>
            <a name="wynajem_dlugoterminowy"></a>
            <p>Przy najmie samochodów firma nie ponosi kosztów wpłaty wstępnej jak przy klasycznym leasingu, a jednocześnie samo zobowiązanie 
                jest rozłożone na względnie krótki okres.</p>
        </div>
    </div>
    <div class="row row_separator">
        <div class="col-xs-12">
            <div class="separator_greyH"></div>
        </div>
    </div>
    <div class="row row_offer">
        <div class="col-md-4 col_l">
            <h2>Wynajem długoterminowy</h2>
        </div>
        <div class="col-md-8 col_r">
            <div class="text">
                <p>Wynajem Długoterminowy  lub raczej Pełny Wynajem Długoterminowy z języka angielskiego Full Service Leasing 
                    (w skrócie FSL) to pojęcie z sektora leasingu samochodowego, który obejmuje pełny wynajem długoterminowy pojazdów 
                    (zawierający przynajmniej trzy inne usługi pozafinansowe, jak np. ubezpieczenie, karty paliwowe, likwidację szkód, 
                    assistance i zawsze - serwis mechaniczny pojazdów.</p>

                <p><b>Podstawowe cechy Pełnego Wynajmu Długoterminowego:</b></p>

                <ul>
                    <li>finansowanie floty Klienta poprzez umowę leasingu operacyjnego, najmu lub dzierżawy,</li>
                    <li>ryzyko wartości rezydualnej (końcowej) jest po stronie Finansującego,</li>
                    <li>czas trwania umowy: w przypadku umowy leasingu operacyjnego minimum 24 miesiące, w przypadku umowy najmu minimum 12 miesięcy </li>
                    <li>umowa musi obejmować przynajmniej trzy usługi pozafinansowe (w tym zawsze serwis mechaniczny).</li>
                </ul>

                <p>Inną formą wynajmu długoterminowego jest Leasing z elementami serwisu - LS (Leasing & Service)</p>

                <p><b>Podstawowe cechy Leasingu z elementami serwisu:</b></p>
                <ul>
                    <li>finansowanie floty Klienta poprzez umowę leasingu operacyjnego, najmu, dzierżawy lub leasingu finansowego,</li>
                    <li>ryzyko wartości rezydualnej (końcowej) po stronie Korzystającego,</li>
                    <li>czas trwania umowy: w przypadku umowy leasingu operacyjnego minimum 24 miesiące, w przypadku umowy leasingu finansowego lub umowy najmu minimum 12 miesięcy,</li>
                    <li>umowa musi obejmować przynajmniej dwie usługi pozafinansowe, bez wymogu serwisu mechanicznego.</li>

                </ul>
                <a name="wynajem_krotkoterminowy"></a>
                <p>Rynek usługi FSL jest atrakcyjny i stale rośnie, ponieważ firmy korzystające z FSL zyskują wymierne korzyści finansowe nie 
                    potrzebują angażować własnych środków finansowych na zakup pojazdu ani środków finansowych niezbędnych do skorzystania z kredytu.</p>
            </div>
        </div>
    </div>
    <div class="row row_offer">
        <div class="col-md-4 col_l">
            <h2>Wynajem krótkoterminowy</h2>
            <small><b>STR</b> (ang. Short Term Rental)</small>
            <small><b>MTR</b> (ang. Medium Term Rental)</small>
        </div>
        <div class="col-md-8 col_r">
            <div class="text">
                <p>Jako wynajem krótkoterminowy rozumie się ciągłe posiadanie środka transportu lub korzystanie z niego przez okres nieprzekraczający 30 dni, 
                    a w przypadku wystąpienia szkody pojazdu flotowego - przez okres nieprzekraczający 90 dni. Z powyższego wynika, że z wynajmem krótkoterminowym 
                    samochodu będziemy mieli do czynienia gdy zostanie on wynajęty na okres maksymalnie do 30 dni.</p>

                <p>Jako wynajem średnioterminowy rozumie się ciągłe posiadanie środka transportu lub korzystanie z niego przez okres nieprzekraczający 
                    24 miesięcy, przy czym minimalny okres korzystania z pojazdu musi wynosić ponad 30 dni. Z powyższego wynika, że z wynajmem 
                    średnioterminowym pojazdu będziemy mieli do czynienia gdy zostanie on wynajęty na okres powyżej 30 dni, 
                    jednak na maksymalnie do 24 miesiące.</p>

                <p>Zgodnie z utrwalony dotychczas poglądem z punktu widzenia podatku dochodowego wynajem taki nie będzie traktowany jak leasing, 
                    w związku z czym opłat za wynajem nie można rozliczać wprost w pełnej wysokości w kosztach uzyskania przychodu. W przypadku najmu 
                    krótkoterminowego obowiązuje ograniczenie wynikające z art. 16 ust. 1 pkt 51 updop, co oznacza, że wydatki z tego tytułu można 
                    zaliczać do kosztów uzyskania przychodu na podstawie „kilometrówki”.</p>

                <p>Konieczność prowadzenia ewidencji przebiegu pojazdów dla samochodów, które są wynajmowane często na bardzo krótki okres nieprzekraczający 
                    kilkudziesięciu dni jest czasem kłopotliwy w zakresie rozliczeń podatkowych i bez dochowania warunku prowadzenia ewidencji przebiegu, opłaty 
                    za wynajęcie samochodu na kilkanaście lub kilkadziesiąt dni w ogóle nie będą mogły zostać zakwalifikowane jako koszty uzyskania przychodu.</p>

                <p><b>Podstawowe cechy Leasingu z elementami serwisu:</b></p>
                <ul>
                    <li>Firma flotowa finansuje samochody w dowolnej formie</li>
                    <li>Wynajem krótkoterminowy - czas trwania umowy do 30 dni</li>
                    <li>Wynajem średnioterminowy - czas trwania umowy od 30 dni do 24 miesięcy</li>
                    <li>Ryzyko RV po stronie finansującego.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row row_offer">
        <div class="col-md-4 col_l">
            <h2>Wartość rezydualna pojazdu, czyli wartość końcowa</h2>
            <small>(ang. Residual Value – RV)</small>
        </div>
        <div class="col-md-8 col_r">
            <div class="text">
                <p>Jest to zakładana wartość rynkowa pojazdu w momencie odsprzedaży, po zakończeniu uzgodnionego okresu użytkowania, czyli po wygaśnięciu 
                    umowy wynajmu długoterminowego lub umowy leasingu.</p>

                <p>Wartość rezydualna maleje wraz z wiekiem auta, a jej wysokość jest uzależniona od kilku czynników – marki i modelu, pojemności silnika, 
                    przebiegu i historii pojazdu oraz (w najmniejszym stopniu) wyposażenia. Obowiązuje tu prosta zasada – im wyższy (i droższy) model auta, 
                    tym większy procent wartości utraci w czasie wielu miesięcy i lat eksploatacji.</p>

                <p>Zwykle oznacza to utratę od 25% do 40% wartości pojazdu po pierwszym roku użytkowania, czyli wartość rezydualna wynosi odpowiednio od 
                    75% do 60%. Dla przedsiębiorcy, korzystającego z kilkudziesięciu czy czasem kilkuset pojazdów, wartość ta przekłada się na wymierne 
                    korzyści lub straty. Dlatego jej właściwe skalkulowanie powinniśmy powierzyć wyspecjalizowanym firmom wynajmu długoterminowego, które 
                    biorą na siebie ryzyko wartości rezydualnej, czyli różnicy między ceną zakupu a wartością końcową floty.</p>
            </div>
        </div>
    </div>
    <div class="row visible-xs">
        <div class="col-xs-12">
            <button class="btn_see_more">Pokaż więcej</button>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $('.btn_see_more').click(function () {
            $('.row_offer').css({'height': 'auto', 'margin-bottom': '55px'});
            $('.btn_see_more').css('display', 'none');
        });
    });
</script>
@endpush
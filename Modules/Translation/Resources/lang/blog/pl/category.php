<?php

return [
    'title' => [
        'category' => 'Kategorie',
        'create category' => 'Utwórz kategorie',
        'edit category' => 'Edytuj kategorie',
    ],
    'button' => [
        'create category' => 'Utwórz kategorie',
    ],
    'table' => [
        'created at' => 'Utwórzono',
        'name' => 'Nazwa',
        'slug' => 'Fragment Url',
    ],
    'form' => [
        'name' => 'Nazwa',
        'slug' => 'Fragment Url',
    ],
    'navigation' => [
        'back to index' => 'Wróć do listy',
    ],
    'list resource' => 'Lista kategorii',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Delete categories',
];

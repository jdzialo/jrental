@extends('layouts.master')
@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>Oferta</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_offer">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-8 text">
            <div class="short">Zarządzanie flotą pojazdów jest procesem logistycznym dotyczącym obsługi środków transportu polegającym na alokacji 
                i przepływie środków transportu polegającym na alokacji i przepływie środków transportu, zasobów ludzkich oraz innych zasobów 
                w powiązaniu z innymi procesami logistycznymi realizowany poprzez uporządkowany zbiór czynności (planowanie, organizowanie, 
                kierowanie i kontrolowanie). Uwzględnia się kryteria: czasu, efektywności spełniania celów organizacji, kosztów oraz lokalizacji.</div>

            <p>Celem zarządzania flotą jest porządkowanie działań mających na celu osiąganie pożądanych wyników jak również wykonywanie zadań 
                przy optymalnym wykorzystaniu floty oraz znacznym obniżeniu jej kosztów.</p>

            <p>CFM odnosi się do całości zagadnień związanych z zarządzaniem środkami transportu – począwszy od ich wyboru, zakupu, przydziału 
                do określonych zadań, poprzez nadzór nad serwisem, po wybót odpowiedniej polityki paliwowej. Do efektywnego zarządzania flotą 
                niezbędne jest stosowanie odpowiedniego oprogramowania komputerowego oraz wprowadzenie polityki flotowej w firmie lub organizacji, 
                do której muszą się zastosować wszyscy użytkownicy pojazdów flotowych.</p>

            <p><b>Podstawowe zadania związane z zarządzaniem flotą to:</b></p>
            <ul>
                <li>wybór i zakup optymalnie dobranych pojazdów,</li>
                <li>wyposażenie dodatkowe, rejestracja, ubezpieczenie, oznakowanie pojazdów,</li>
                <li>dysponowanie pojazdami oraz delegowanie ich do określonych zadań,</li>
                <li>zapewnienie wysokiej dostępności floty,</li>
                <li>koordynowanie działań,</li>
                <li>kontrolowanie wykorzystania floty oraz ich stanu lub nadużyć</li>
            </ul>
            <br/>
            <p><b>Podstawowe cechy zrządzania flotą:</b></p>
            <ul>
                <li>brak finansowania floty Klienta poprzez firmę zarządzającą flotą,</li>
                <li>usługa zarządzania flotą dotyczy pojazdów będących własnością klienta,</li>
                <li>umowa na zarządzanie flotą zawarta na minimum 12 miesięcy,</li>
                <li>dowolna liczba usług pozafinansowych</li>
            </ul>

            <p>Przykładowe usługi pozafinansowe występujące w wynajmie długoterminowym pojazdów: Większość Klientów firm oferujących usługi flotowe 
                użytkuje pojazdy, korzystając z kompleksowego pakietu usług. Obejmuje on, obok finansowania, również serwisowanie oraz zarządzanie 
                flotą samochodów w każdym zakresie – operacyjnym, administracyjnym, prawnym i technicznym.</p>

            <p><b>Do głównych usług pozafinansowych należą:</b></p>
            <ul>
                <li>ubezpieczenie,</li>
                <li>likwidacja szkód,</li>
                <li>serwisowanie,</li>
                <li>pojazdy zastępcze,</li>
                <li>pojazdy pomostowe,</li>
                <li>wymiana i serwis opon,</li>
                <li>assistance i obsługa zdarzeń,</li>
                <li>obsługa kart paliwowych,</li>
                <li>door-to-door,</li>
                <li>raportowanie.</li>
            </ul>
        </div>
        <div class="col-sm-5 col-md-4">
            @include('partials.offer-ask', ['txt' => "Masz pytania dotyczące CFM ?"])
        </div>
    </div>
</div>
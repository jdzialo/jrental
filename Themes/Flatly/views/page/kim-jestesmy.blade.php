@extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@endsection
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@endsection

@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>{{ $page->title }}</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_kim_jestesmy">
    {!! $page->body !!}
    <div class="visible-xs col-xs-12 wrapper_see_more">
        <button class="btn_see_more">Pokaż więcej</button>
    </div>
</div>
@endsection


@push('scripts')
<script>
    $(document).ready(function () {
        $('.btn_see_more').click(function () {
            $('p.see_more').css({'height': 'auto'});
            $('.btn_see_more').css('display', 'none');
        });
    });
</script>
@endpush
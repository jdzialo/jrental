<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>Kim jesteśmy</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_kim_jestesmy">
    <div class="row">
        <div class="col-xs-12 text_col2">
            <div class="text">
                <div class="short">Jesteśmy jedną z najnowocześniejszych i najdłużej działających na rynku polskim niezależnych firm wynajmu 
                    i obsługi flotowej pojazdów.</div>
                <p>Swoją działalność rozpoczęliśmy pod koniec lat 90-tych XX wieku i byliśmy jedną z pierwszych prywatnych grup oferujących swoim 
                    klientom usługę full service leasing wraz z usługami dodatkowymi opartymi o nowoczesne technologie teleinformatyczne. W okresie 
                    początkowym oferta nasza była głównie skierowana do międzynarodowych firm technicznych, technologicznych i farmaceutycznych, 
                    które z usługą full service leasing spotkały się w innych regionach świata, i które doskonale zdawały sobie sprawę z korzyści 
                    jakie daje outsourcing wynajmu i obsługi pojazdów wykorzystywanych we własnej działalności gospodarczej.</p>
                <p>Firmy te wykorzystywały synergię płynącą z podziału ról, skupiając się wyłącznie na swojej podstawowej działalności, oddając 
                    wszelkie sprawy związane z wyborem, zakupem, obsługą serwisową czy likwidacją szkód wynajętym specjalistom z branży CFM, czyli 
                    Car Fleet Management.</p>
                <p>Począwszy od 2004 roku, wraz z wejściem Polski do Unii Europejskiej coraz więcej firm z rodzimym polskim kapitałem decydowało 
                    się na skorzystanie z usług wynajmu i obsługi floty pojazdów oferowanych przez firmy z naszej grupy kapitałowej, redukując 
                    w ten sposób koszty funkcjonowania swoich działów handlowych, logistyki i transportu. Pod koniec pierwszej dekady XXI wieku 
                    obsługiwaliśmy już kilka tysięcy pojazdów.</p>
                <p>Lata doświadczeń i współpracy z szeroką siecią dostawców i usługodawców zlokalizowanych na terenie całego kraju, 
                    do których zaliczają się m.in.: producenci i dealerzy samochodów, autoryzowane sieci serwisowe, warsztaty blacharskie, 
                    warsztaty oponiarskie i koncerny paliwowe, firmy i towarzystwa ubezpieczeniowe, ale także, co może budzić pewną ciekawość, 
                    firmy z sektora telekomunikacyjnego i informatycznego, pozwoliły nam na opracowanie autorskich metod optymalizacji kosztów 
                    pojazdów wykorzystywanych w szeroko rozumianej działalności gospodarczej.</p>
            </div>
        </div>
    </div>
    <div class="row row_offer">
        <div class="col-xs-12 col_offer">
            <p>Nasze rozwiązania są skuteczne i funkcjonalne zarówno w przypadku dużych flot liczących ponad 
                200 pojazdów, ale także dają doskonałe wyniki w przypadku flot kilkupojazdowych czy wręcz pojedynczych pojazdów wykorzystywanych we własnej działalności gospodarczej przez 
                osoby fizyczne i małe spółki cywilne.</p>
            <a href="#" class="btn_blue">Sprawdź ofertę <i class="fa fa-caret-right" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text_col2">
            <div class="text">
                <p>Firma <b>QWANT</b> w ramach swojej działalności oferuje usługi związaną z dostawą, doposażeniem, wynajmem, zarządzaniem i 
                    obsługą flotową pojazdów wykorzystywanych przez jej klientów w prowadzonej przez nich działalności gospodarczej. W ramach 
                    usługi JRENTAL program wynajmu i obsługi flotowej dostępny jest także dla osób fizycznych działających w ramach sieci MLM 
                    oferującej na rynku europejskim, w tym m.in. w Polsce, Czechach i na Słowacji, produkty firmy Jeunesse® Global (USA), 
                    który został opracowany i powiązany z programem wsparcia marketingowego i obejmuje swoim zasięgiem zarówno wybór, zakup jak 
                    i dostawę i obsługę serwisową pojazdów flotowych oznakowanych logiem firmy Jeunesse®, które mają służyć managerom, 
                    współpracownikom oraz aktywnym dystrybutorom produktów w ich codziennej pracy.</p>
                <p>Wszystkie pojazdy parku samochodowego QFM, objęte są specjalnymi przywilejami w zakresie dostępności do usług serwisowych, 
                    oraz dają możliwość uzyskania wysokich rabatów na usługi związane z ich obsługą techniczną. Klienci QFM mają stały dostęp 
                    do zespołu fachowców z branży motoryzacyjnej i ubezpieczeniowej, posiadających wieloletnie doświadczenie.</p>
            </div>
        </div>
    </div>
</div>
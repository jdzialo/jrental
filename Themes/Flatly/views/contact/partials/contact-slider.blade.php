<div class="box_contact_slider">
    @foreach($slider->files()->get() as $item)
    <div class="box_contact_slide">
        <div class="foto_box">
            <?php  if(!empty($item->id))  :  ?>
                <img class="foto" src="{{ Imagy::getThumbnail($item->path, 'blogThumb') }}" alt="{{$item->id}}"/>
            <?php else: ?>
               
            <?php endif; ?>
        </div>
    </div>
    @endforeach
</div>

@push('style')
{!! Theme::style('vendor/slick-carousel/slick/slick.css') !!}
@endpush
@push('scripts')
{!! Theme::script('vendor/slick-carousel/slick/slick.min.js') !!}
<script>
    $(document).ready(function () {
        $('.box_contact_slider').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            speed: 300,
            //autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            prevArrow: '<div class="prevArrow"><i class="fa fa-caret-left"></i></div>',
            nextArrow: '<div class="nextArrow"><i class="fa fa-caret-right"></i></div>'
        });
    });
</script>
@endpush

<div class="row">
    <div class="col-xs-12 bluecontainerslim">
        <div class="editarea">
            {{ trans('page::contact.contact.receive message') }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class='form-group{{ $errors->has("firstname") ? ' has-error' : '' }}'>
            {!! Form::text("firstname", old("firstname"), ['onfocus' => 'this.placeholder="trans("page::contact.form.firstname")"', 'onblur' => 'this.placeholder="trans("page::contact.form.firstname")"', 'placeholder' => trans('page::contact.form.firstname')]) !!}
            {!! $errors->first("firstname", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class='form-group{{ $errors->has("lastname") ? ' has-error' : '' }}'>
            {!! Form::text("lastname", old("lastname"), ['onfocus' => 'this.placeholder="trans("page::contact.form.lastname")"', 'onblur' => 'this.placeholder="trans("page::contact.form.lastname")"', 'placeholder' => trans('page::contact.form.lastname')]) !!}
            {!! $errors->first("lastname", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class='form-group{{ $errors->has("phone") ? ' has-error' : '' }}'>
            {!! Form::text("phone", old("phone"), ['onfocus' => 'this.placeholder="trans("page::contact.form.phone")"', 'onblur' => 'this.placeholder="trans("page::contact.form.phone")"', 'placeholder' => trans('page::contact.form.phone')]) !!}
            {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
            {!! Form::text("email", old("email"), ['onfocus' => 'this.placeholder="trans("page::contact.form.email")"', 'onblur' => 'this.placeholder="trans("page::contact.form.email")"', 'placeholder' => trans('page::contact.form.email')]) !!}
            {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class='form-group{{ $errors->has("adress") ? ' has-error' : '' }}'>
            {!! Form::text("adress", old("adress"), ['onfocus' => 'this.placeholder="trans("page::contact.form.adress")"', 'onblur' => 'this.placeholder="trans("page::contact.form.adress")"', 'placeholder' => trans('page::contact.form.adress')]) !!}
            {!! $errors->first("adress", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class='form-group{{ $errors->has("adress_cd") ? ' has-error' : '' }}'>
            {!! Form::text("adress_cd", old("adress_cd"), ['onfocus' => 'this.placeholder="trans("page::contact.form.adress_cd")"', 'onblur' => 'this.placeholder="trans("page::contact.form.adress_cd")"', 'placeholder' => trans('page::contact.form.adress_cd')]) !!}
            {!! $errors->first("adress_cd", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class='form-group{{ $errors->has("place") ? ' has-error' : '' }}'>
            {!! Form::text("place", old("place"), ['onfocus' => 'this.placeholder="trans("page::contact.form.place")"', 'onblur' => 'this.placeholder="trans("page::contact.form.place")"', 'placeholder' => trans('page::contact.form.place')]) !!}
            {!! $errors->first("place", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class='form-group{{ $errors->has("voivodeship") ? ' has-error' : '' }}'>
            {!! Form::text("voivodeship", old("voivodeship"), ['onfocus' => 'this.placeholder="trans("page::contact.form.voivodeship")"', 'onblur' => 'this.placeholder="trans("page::contact.form.voivodeship")"', 'placeholder' => trans('page::contact.form.voivodeship')]) !!}
            {!! $errors->first("voivodeship", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class='form-group{{ $errors->has("zip_code") ? ' has-error' : '' }}'>
            {!! Form::text("zip_code", old("zip_code"), ['onfocus' => 'this.placeholder="trans("page::contact.form.zip_code")"', 'onblur' => 'this.placeholder="trans("page::contact.form.zip_code")"', 'placeholder' => trans('page::contact.form.zip_code')]) !!}
            {!! $errors->first("zip_code", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class='form-group{{ $errors->has("country") ? ' has-error' : '' }}'>
            {!! Form::text("country", old("country"), ['onfocus' => 'this.placeholder="trans("page::contact.form.country")"', 'onblur' => 'this.placeholder="trans("page::contact.form.country")"', 'placeholder' => trans('page::contact.form.country')]) !!}
            {!! $errors->first("country", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row row_textarea">
    <div class="col-xs-12">
        <div class='form-group{{ $errors->has("message") ? ' has-error' : '' }}'>
            {!! Form::textarea("message", old("message"), ['onfocus' => 'this.placeholder="trans("page::contact.form.message")"', 'onblur' => 'this.placeholder="trans("page::contact.form.message")"',  'placeholder' => trans('page::contact.form.message')]) !!}
            {!! $errors->first("message", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <button type="submit" class="btn_blue">Wyślij wiadomość <i class="fa fa-caret-right" aria-hidden="true"></i></button>
    </div>
</div>
<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateOfferFormRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
         
        'name' => 'required',
        'email' => 'required|email',
        'instalation_location' => 'required',
        'phone' => 'required|numeric',   
       
        
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => trans('page::contact.name is required'),
            'instalation_location.required' => trans('page::contact.instalation_location is required'),
            'phone.required' => trans('page::contact.phone is required'),
            'email.required' => trans('page::contact.email is required'),
            'email.email' => trans('page::contact.email is not valid'),
            'phone.numeric' => trans('page::contact.phone is not valid'),
           
           
        ];
    }
}


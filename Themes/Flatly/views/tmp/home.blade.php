@if(Session::has('message'))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <a class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                <p>{{Session::get('message')}}</p>
            </div>
        </div>
    </div>
</div>
@endif

<div class="container-fluid box_main_foto">
    <div class="big">Rent Comfort and Security</div>
    <div class="container cbtn_main">
        <div class="">
            <div class="row rbtn_main">
                <div class="col-sm-4 col_btn_main">
                    <a href="/oferta#wynajem_dlugoterminowy" class="btn_main">
                        Wynajem <b>długoterminowy</b>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-sm-4 col_btn_main">
                    <a href="/oferta#wynajem_krotkoterminowy" class="btn_main">
                        Wynajem <b>krótkoterminowy</b>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-sm-4 col_btn_main">
                    <a href="/zarzadzanie-flota" class="btn_main"> 
                        Zarządzanie <b>flotą</b>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container page_home">
    <div class="row page_header">
        <div class="col-xs-12">
            <h1 class="home_title">Czym jest <b>jrental?</b></h1>
        </div>
    </div>
    <div class="row page_content">
        <div class="col-xs-12 text_col2">
            <div class="text">
                <div class="short">JRENTAL to kompleksowy i dedykowany program wynajmu oraz obsługi pojazdów, który jest dostępny dla aktywnych 
                    dystrybutorów produktów amerykańskiej firmy JEUNESSE® GLOBAL działających na terenie Polski, Czech i Słowacji.</div>
                <div class="p">Firma Jeunesse® Global (USA) to jedna z największych sieci dystrybucji produktów opartych o komórki macierzyste oraz suplementów 
                    diety stworzonych na bazie wyciągów z roślin, owoców i warzyw. Jest jedną z najdynamiczniej rozwijających się firm z tego sektora w świecie. 
                    W Europie jest obecna w większości krajów. W Polsce firma działa od 2015 roku i skupia ponad 4.000 osób aktywnie zajmujących się promocją 
                    i dystrybucją produktów Jeunesse®.</div>
                <p>W ramach współpracy nawiązanej pomiędzy przedstawicielami i managerami firmy JEUNESSE® GLOBAL oraz firmy QWANT został stworzony program 
                    wsparcia sprzedaży oraz optymalizacji kosztów i czasu obsługi pojazdów wykorzystywanych przy realizacji zadań związanych z dystrybucją 
                    produktów Jeunesse® .</p>
                <p>Krótko mówiąc JRENTAL® to program zakupu, finansowania, wynajmu, obsługi serwisowej i wymiany pojazdów użytkowanych przez managerów 
                    i dystrybutorów produktów Jeunesse® poruszających się po drogach całej Europy. JRENTAL® , to komfort, wygoda, bezpieczeństwo 
                    i profesjonalizm obsługi skumulowany w jednym miejscu. Partnerami projektu JRENTAL® dostarczającymi i obsługującymi serwisowo 
                    pojazdy są LEXUS Polska  i Toyota Polska.</p>
            </div>
        </div>
    </div>
</div>

@include('partials.slider')

@push('scripts')
<script>
    $(document).ready(function () {
        $('#myModal').modal('show');
    });
</script>
@endpush
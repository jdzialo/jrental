@extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@endsection
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@endsection

@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>{{ $page->title }}</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_offer pg_zarzadzanie_flota">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-8 text">
            {!! $page->body !!}
        </div>
        <div class="col-xs-12 col-sm-5 col-md-4">
            @include('partials.offer-ask', ['txt' => "Masz pytania dotyczące CFM ?"])
        </div>
    </div>
</div>
<div style="height: 30px;"></div>
@endsection



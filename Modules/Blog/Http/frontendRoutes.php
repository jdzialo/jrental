<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => 'blog'], function (Router $router) {
    $locale = LaravelLocalization::setLocale() ?: App::getLocale();
    
    $router->get('aktualnosci', [
        'as' => $locale . '.blog',
        'uses' => 'PublicController@index',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
    $router->get('aktualnosci/{slug}', [
        'as' => $locale . '.blog.slug',
        'uses' => 'PublicController@show',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
});

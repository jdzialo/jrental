@extends('layouts.master')
@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>Oferta</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        @for ($i = 0; $i < 12; $i++)
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="box_slide">
                <div class="foto_box">
                    <img class="foto" src="/themes/flatly/img/tmp/slide_01.jpg">
                    <img class="rect_round" src="/themes/flatly/img/icons/rect_round.png" >
                    <!--<div class="label_star"><i class="fa fa-star" aria-hidden="true"></i></div>-->
                </div>
                <div class="content_box">
                    <div class="data">12.08.2016</div>
                    <div class="title">JRental przygotował pierwsze Lexusy dla członków Jeunesse</div>
                    <a href="#" class="btn_gray">Przeczytaj cały wpis</a>
                </div>
            </div>
        </div>
        @endfor
    </div>
</div>



var gulp = require("gulp");
var shell = require('gulp-shell');
var elixir = require('laravel-elixir');
var themeInfo = require('./theme.json');
//elixir.config.assetsPath = './resources';
process.env.DISABLE_NOTIFIER = true;

//var task = elixir.Task;
//elixir.extend("stylistPublish", function() {
//    new task("stylistPublish", function() {
//        return gulp.src("").pipe(shell("php ../../artisan stylist:publish " + themeInfo.name));
//    }).watch("**/*.less");
//});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    /**
     * Compile less
     */
    mix.less([
            "asgard.less"
        ], 'assets/css/asgard.css')
    .copy('assets/css', '../../public/themes/' + themeInfo.name.toLowerCase() + '/css');

    /**
     * Concat scripts
     */
    mix.scripts([
        "main.js"
    ], 'assets/js/main.js')
    .copy('assets/js', '../../public/themes/' + themeInfo.name.toLowerCase() + '/js' );
    
});

@extends('layouts.master')
@section('content')

<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>Kontakt</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container pg_contact">
    <div class="row row_data">
        <div class="col-sm-12 col-md-5 col-lg-6 text">
            <b>QWANT Spółka z o.o.</b> (nazwa skrócona)<br/>
            ul. Puławska 405A<br/>
            02-801 Warszawa<br/>
            Regon: 200455020<br/>
            NIP: 966-20-70-760<br/><br/>

            <b>Tel.</b> +48 725 20 77 80<br/>
            <b>Fax</b> +48 22 754 6456<br/>
            <b>e-mail:</b> <a href="mailto:biuro@qwant.pl">biuro@qwant.pl</a>  (lub <a href="mailto:info@qwant.pl">info@qwant.pl</a>)
        </div>
        <div class="col-sm-12 col-md-7 col-lg-6">
            <div class="row_slider_map">
                <div class="hidden-xs col_slider">
                    @include('contact.partials.contact-slider', ['slider' => $slider])
                </div><div class="col_gmap">
                    <div id='gmap'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row_content">
        <div class="col-xs-12">
            QWANT Spółka z ograniczoną odpowiedzialnością z siedzibą w Warszawie, prowadzącą działalność pod adresem 02-801 Warszawa, ul. Puławska 405A, zarejestrowana przez Sąd Rejonowy 
            m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000390867, Regon 200455020, NIP: 966-20-70-760, 
            kapitał zakładowy 505.000,00 złotych, opłacony w całości w gotówce.
        </div>
    </div>
    <div class="row row_frm">
        <div class="col-xs-12">
            <h2>Formularz kontaktowy:</h2>
            {!! Form::open(['route' => 'contactStore', 'method' => 'post', 'class' => 'contactform']) !!}
                @include('contact.partials.create-fields', ['lang' => locale(), 'errors' => $errors])
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxrjy4S3oZppA94ztine1IIr2oAdx4NPc&callback=initialize"></script>
<script>
    var map;
    var geocoder = new google.maps.Geocoder();
    var ib;

    function initialize() {
        var mapOptions = {
            center: {lat: 50.397, lng: 18.644},
            zoom: 14,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.DEFAULT,
            },
            disableDoubleClickZoom: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            },
            scaleControl: true,
            scrollwheel: true,
            panControl: true,
            streetViewControl: true,
            draggable: true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
        getAdress();
    }
    function getAdress() {
        var adress = "Puławska 405A, Warszawa";
        geocoder.geocode({'address': adress}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    icon: '/themes/flatly/img/icons/ico_gm_marker.png'
                });
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endpush

<?php

namespace Modules\Slider\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateSlideRequest extends BaseFormRequest {

    protected $translationsAttributesKey = 'slider::sliders.validation.attributes';

    public function rules() {
        return [
        ];
    }

    public function translationRules() {
        return [
           //TODO: make update validation
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
        ];
    }

    public function translationMessages() {
        return [
            'title.required' => trans('slider::messages.title is required')
        ];
    }

}

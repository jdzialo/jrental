<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (!App::runningInConsole()) {
    $router->get('/', [
        'uses' => 'PublicController@homepage',
        'as' => 'homepage',
        'middleware' => config('asgard.page.config.middleware'),
        'middleware' => 'can:dashboard.index',// @isinfo: hasło do strony
    ]);

    $router->get("strona-kontaktowa", [
        'uses' => 'PublicController@contactpage',
        'as' => 'contact',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
    $router->post('strona-kontaktowa/store', [
        'uses' => 'PublicController@storecontact',
        'as' => 'contactStore',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
    $router->any('{uri}', [
        'uses' => 'PublicController@uri',
        'as' => 'page',
        'middleware' => config('asgard.page.config.middleware'),
    ])->where('uri', '.*');


    $router->get("gallery", [
        //'as' => $locale . '.gallery',
        'uses' => 'PublicController@gallery',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
}

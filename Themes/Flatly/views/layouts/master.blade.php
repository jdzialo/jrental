<!DOCTYPE html>
<html>
    <head lang="{{ LaravelLocalization::setLocale() }}">
        <meta charset="UTF-8">
        @section('meta')
        <meta name="description" content="@setting('core::site-description')" />
        @show
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            @section('title')@setting('core::site-name')@show
        </title>
        <link rel="shortcut icon" href="{{ Theme::url('favicon.ico') }}">

        @stack('style')
        {!! Theme::style('css/main.css') !!}

    </head>
    <body>
        @include('partials.navigation')

        <div class="container-fluid">
            <a href="/assets/media/jrent_wniosek_wynajem_pojazdu_2016.pdf" target="_blank">
                <div class="boxBarMedia wniosek">
                    <div class="barMedia"><b>WNIOSEK</b></div>
                </div>
            </a>
        </div>  
        <div class="container-fluid">
            <a href="http://wizyty.euromaster.pl" target="_blank">
                <div class="boxBarMedia serwis">
                    <div class="barMedia"><b>SERWIS OPON</b></div>
                </div>
            </a>
        </div>  
        @yield('content')
        @include('partials.footer')

        {!! Theme::script('js/all.js') !!}
        @stack('scripts')

        <?php if (Setting::has('core::analytics-script')): ?>
            {!! Setting::get('core::analytics-script') !!}
        <?php endif; ?>
    </body>
</html>



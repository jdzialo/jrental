@extends('layouts.master')

@section('title')
Blog posts | @parent
@stop

@section('content')
<div class="container-fluid pg_title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 pg_title_content">
                <h1>Aktualności</h1>
                <div class="box_blue_grad"></div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        @if (isset($posts))
        @foreach($posts as $post)
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="margin: 15px 0">
            <div class="box_slide">
                <div class="foto_box">
                    @if($post->files()->where('zone', 'thumbnail')->get()->first())
                    <img class="foto" src="{{ $post->files()->where('zone', 'thumbnail')->get()->first()->path }}" >
                    @else
                    @if($post->getThumbnailAttribute() != '')
                    <img class="foto" src="{{ $post->getThumbnailAttribute()->getThumbnail('blogThumb') }}" >
                    @endif
                    @endif
                    <img class="rect_round" src="/themes/flatly/img/icons/rect_round.png" >
                    <!--<div class="label_star"><i class="fa fa-star" aria-hidden="true"></i></div>-->
                </div>
                <div class="content_box">
                    <div class="data">{{ $post->created_at->format('d-m-Y') }}</div>
                    <div class="title">{{ $post->title }}</div>
                    <a href="{{ URL::route($currentLocale . '.blog.slug', [$post->slug]) }}" class="btn_gray">Przeczytaj cały wpis</a>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>

    <div class="row box_pagination">
        <div class="col-xs-12">
            {!! with(new App\Pagination\HDPresenter($posts))->render(); !!} 
        </div>
    </div>

</div>
<div style="height: 30px"></div>
@stop


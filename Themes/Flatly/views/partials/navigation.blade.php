<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <a class="logo_jrental_top" href="{{ URL::to('/') }}">
            <img src="/themes/flatly/img/logo_jrental_top.png">
        </a><a class="logo_jeunesse_top" href="{{ URL::to('/') }}">
            <img src="/themes/flatly/img/logo_jeunesse_top.png">
        </a>
        <button type="button" class="navbar-toggle btn_toggle_login" data-toggle="collapse" aria-expanded="false" aria-controls="navbar-login-collapse" data-target="#navbar-login-collapse">
            <i class="fa fa-user" aria-hidden="true"></i>
        </button>
        <button type="button" class="navbar-toggle btn_toggle_menu" data-toggle="collapse" aria-expanded="false" aria-controls="navbar-menu-collapse" data-target="#navbar-menu-collapse">
            <i class="fa fa-bars" aria-hidden="true"></i> MENU
        </button>
    </div>
    <div id="navbar-login-collapse" class="navbar-collapse box_nav_buttons collapse pull-right">
        <ul class="nav navbar-nav">
            <li>
                <a href="#" class="btn_register">Zarejestruj się</a>
            </li>
            <li>
                <a href="#" class="btn_login">Zaloguj się</a>
            </li>
        </ul>
    </div>
    <div id="navbar-menu-collapse" class="navbar-collapse collapse pull-right">
        @menu('Menu')
    </div>
</nav>
<div class="menu_bufor"></div>
@push('scripts')
<script>
    $(document).ready(function () {
        $('button.btn_toggle_login').click(function() {
            //$('#navbar-menu-collapse').removeClass('in');
            $('#navbar-menu-collapse').collapse('hide');
        });
        $('button.btn_toggle_menu').click(function() {
            //$('#navbar-login-collapse').removeClass('in');
            $('#navbar-login-collapse').collapse('hide');
        });
        $(window).scroll(function () {
            var scrolled = $(document).scrollTop().valueOf();
            var container = $('nav.navbar.navbar-default');
            if (scrolled >= 1) {
                //container.css('top', '0px');
                container.css('box-shadow', '0 7px 14px #ebebeb');
            } else {
                //container.css('top', '-100px');
                container.css('box-shadow', '0 0 0 #ebebeb');
            }
        });
    });
</script>
@endpush

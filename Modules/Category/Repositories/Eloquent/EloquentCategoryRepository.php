<?php

namespace Modules\Category\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Category\Events\CategoryWasCreated;
use Modules\Category\Repositories\CategoryRepository;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
    public function create($data)
    {
        $category = $this->model->create($data);

        event(new CategoryWasCreated($category));

        return $category;
    }

    public function update($category, $data)
    {
        $category->update($data);

        return $category;
    }

    /**
     * Get all online categories
     * @return object
     */
    public function allOnline()
    {
        $locale = App::getLocale();

        return $this->model->whereHas('translations', function (Builder $q) use ($locale) {
            $q->where('locale', "$locale");
            $q->where('status', 1);
        })->with('translations')->orderBy('created_at', 'DESC')->get();
    }
}

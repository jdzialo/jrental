<?php

namespace Modules\Category\Http\Controllers\Api;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Category\Services\CategoryOrdener;

class CategoryItemController extends Controller
{
    /**
     * @var Repository
     */
    private $cache;
    /**
     * @var CategoryOrdener
     */
    private $categoryOrdener;
    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;

    public function __construct(CategoryOrdener $categoryOrdener, Repository $cache, CategoryItemRepository $categoryItem)
    {
        $this->cache = $cache;
        $this->categoryOrdener = $categoryOrdener;
        $this->categoryItem = $categoryItem;
    }

    /**
     * Update all category items
     * @param Request $request
     */
    public function update(Request $request)
    {
        $this->cache->tags('categoryItems')->flush();

        $this->categoryOrdener->handle($request->get('category'));
    }

    /**
     * Delete a category item
     * @param Request $request
     * @return mixed
     */
    public function delete(Request $request)
    {
        $categoryItem = $this->categoryItem->find($request->get('categoryitem'));

        if (! $categoryItem) {
            return Response::json(['errors' => true]);
        }

        $this->categoryItem->destroy($categoryItem);

        return Response::json(['errors' => false]);
    }
}

<?php

return [
    'title' => [
        'slider' => 'Slider',
        'edit slider' => 'Edit slider',
        'create slider' => "Add slider"
    ],
    'form' => [
        'title' => 'Title'
    ],
    'button' => [
        'create slider' => "Create slider"
    ],
    'popup' => [
        'slide_content' => 'Slider description',
        'title' => 'Title',
        'content' => 'Description',
        'url' => 'Link',
    ],
];

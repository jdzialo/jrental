<?php

namespace Modules\Category\Repositories\Cache;

use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Category\Repositories\CategoryItemRepository;

class CacheCategoryItemDecorator extends BaseCacheDecorator implements CategoryItemRepository
{
    /**
     * @var CategoryItemRepository
     */
    protected $repository;

    public function __construct(CategoryItemRepository $categoryItem)
    {
        parent::__construct();
        $this->entityName = 'categoriesItems';
        $this->repository = $categoryItem;
    }

    /**
     * Get all root elements
     *
     * @param  int   $categoryId
     * @return mixed
     */
    public function rootsForCategory($categoryId)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.rootsForCategory.{$categoryId}", $this->cacheTime,
                function () use ($categoryId) {
                    return $this->repository->rootsForCategory($categoryId);
                }
            );
    }

    /**
     * Get the category items ready for routes
     *
     * @return mixed
     */
    public function getForRoutes()
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.getForRoutes", $this->cacheTime,
                function () {
                    return $this->repository->getForRoutes();
                }
            );
    }

    /**
     * Get the root category item for the given category id
     *
     * @param  int    $categoryId
     * @return object
     */
    public function getRootForCategory($categoryId)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.getRootForCategory.{$categoryId}", $this->cacheTime,
                function () use ($categoryId) {
                    return $this->repository->getRootForCategory($categoryId);
                }
            );
    }

    /**
     * Return a complete tree for the given category id
     *
     * @param  int    $categoryId
     * @return object
     */
    public function getTreeForCategory($categoryId)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.getTreeForCategory.{$categoryId}", $this->cacheTime,
                function () use ($categoryId) {
                    return $this->repository->getTreeForCategory($categoryId);
                }
            );
    }

    /**
     * Get all root elements
     *
     * @param  int    $categoryId
     * @return object
     */
    public function allRootsForCategory($categoryId)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.allRootsForCategory.{$categoryId}", $this->cacheTime,
                function () use ($categoryId) {
                    return $this->repository->allRootsForCategory($categoryId);
                }
            );
    }

    /**
     * @param  string $uri
     * @param  string $locale
     * @return object
     */
    public function findByUriInLanguage($uri, $locale)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.findByUriInLanguage.{$uri}.{$locale}", $this->cacheTime,
                function () use ($uri, $locale) {
                    return $this->repository->findByUriInLanguage($uri, $locale);
                }
            );
    }
}

<div class='container'>
    <div class="row">
        <div class="bluecontainer whitefont">
            <div class="row">

                <div class="col-sm-12 text-center titleform-xs">
                    Rampa dla Ciebie - zapytaj o ofertę
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div class='form-group errormessage {{ $errors->has("name") ? ' has-error' : '' }}'>
                                {!! Form::text("name", old("name"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Imie i Nazwisko"', 'placeholder' => trans('page::contact.form.name')]) !!}
                                {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <div class='form-group errormessage {{ $errors->has("phone") ? ' has-error' : '' }}'>      
                                {!! Form::text("phone", old("phone"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Telefon"',  'placeholder' => trans('page::contact.form.phone')]) !!}
                                {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class='form-group errormessage {{ $errors->has("email") ? ' has-error' : '' }}'>
                                {!! Form::email("email", old("email"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="E-mail"',  'placeholder' => trans('page::contact.form.email')]) !!}
                                {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <div class='form-group errormessage {{ $errors->has("instalation_location") ? ' has-error' : '' }}'>
                        {!! Form::textarea("instalation_location", old("message"), ['class' => 'form-control circle-input-textarea', 'rows' => '4', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Opis miejsca instalacji"',  'placeholder' => trans('page::contact.form.instalation_location')]) !!}
                        {!! $errors->first("instalation_location", '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-9"></div>
                        <div class="col-sm-3">
                            {!! Form::submit(trans('page::contact.button.send'), 
                            array('class'=>'btn btn-default btn-blue')) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-1"></div>

            </div>
        </div>
    </div>
</div>

<!--<div class="container">
    <div class="box-body">
        <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
            {!! Form::text("name", old("name"), ['class' => 'form-control', 'placeholder' => trans('page::contact.form.name')]) !!}
            {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
        </div>

        <div class='form-group{{ $errors->has("phone") ? ' has-error' : '' }}'>      
            {!! Form::text("phone", old("phone"), ['class' => 'form-control',  'placeholder' => trans('page::contact.form.phone')]) !!}
            {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
        </div>

        <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
            {!! Form::email("email", old("email"), ['class' => 'form-control',  'placeholder' => trans('page::contact.form.email')]) !!}
            {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
        </div>
        <div class='form-group{{ $errors->has("instalation_location") ? ' has-error' : '' }}'>
            {!! Form::textarea("instalation_location", old("message"), ['class' => 'form-control',  'placeholder' => trans('page::contact.form.instalation_location')]) !!}
            {!! $errors->first("instalation_location", '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group">
            {!! Form::submit(trans('page::contact.button.send'), 
            array('class'=>'btn btn-primary')) !!}
        </div>
    </div>
</div>-->


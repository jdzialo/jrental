
{!! Form::open(['route' => 'offerStore', 'method' => 'post']) !!}
@include('offer.partials.create-fields', ['lang' => locale(), 'errors' => $errors])
{!! Form::close() !!}

@push('scripts')
{!! Theme::script('js/focusblurform.js') !!}
@endpush

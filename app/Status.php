<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Status is setted to constructor by IoC pattern, so even object will have an status
 * @author Ivan Dublianski <ivan.dublianski@gmail.com>
 * @since 2016
 * @package App
 */
class Status extends Model {
    const ACTIVE = 1;
    const INACTIVE = 2;
    
    
    const ACTIVE_CLASS = 'success';
    const INACTIVE_CLASS = 'danger';
    
    /**
     * @var array
     */
    private $statuses = [];

    public function __construct() {
        $this->statuses = [
            self::ACTIVE => trans('core::core.status.active'),
            self::INACTIVE => trans('core::core.status.inactive'),
        ];
        
        $this->classes = [
            self::ACTIVE => self::ACTIVE_CLASS,
            self::INACTIVE => self::INACTIVE_CLASS,
        ];
    }

    /**
     * Get the available statuses
     * @return array
     */
    public function lists() {
        return $this->statuses;
    }
    
    /**
     * Get the statuses classes
     * @return array
     */
    public function classes() {
        return $this->classes;
    }

    /**
     * Get the post status
     * @param int $statusId
     * @return string
     */
    public function get($statusId) {
        if (isset($this->statuses[$statusId])) {
            return $this->statuses[$statusId];
        }

        return $this->statuses[self::INACTIVE];
    }
    
}

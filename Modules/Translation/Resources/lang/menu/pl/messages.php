<?php

return [
    /* Menu management */
    'menu created' => 'Menu zostało utworzone',
    'menu not found' => 'Menu nie został znaleziony',
    'menu updated' => 'Menu zostało zaktualizowane',
    'menu deleted' => 'Menu zostało usunięte.',
    /* MenuItem management */
    'menuitem created' => 'Element menu utworzony',
    'menuitem not found' => 'Element menu nie został znaleziony',
    'menuitem updated' => 'Element menu zaktualizowany.',
    'menuitem deleted' => 'Element menu usunięty.',
];

<?php

namespace Modules\Page\Events;

use Modules\Media\Contracts\DeletingMedia;

class PageWasDeleted implements DeletingMedia
{
     /**
     * @var string
     */
    private $pageClass;
    /**
     * @var int
     */
    private $pageId;
    /**
     * @var object
     */
    public $page;

    public function __construct($page)
    {
        $this->page = $page;
        $this->pageId = $page->id;
        $this->postClass = get_class($page);
    }
    
     /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->pageId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return $this->pageClass;
    }
}

<?php

return [
    'title' => [
        'slider' => 'Slider',
        'edit slider' => 'Edytuj slider',
        'create slider' => "Dodaj slider"
    ],
    'form' => [
        'title' => 'Tytuł'
    ],
    'button' => [
        'create slider' => "Utwórz slider"
    ],
    'popup' => [
        'slide_content' => 'Opis slajdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
];

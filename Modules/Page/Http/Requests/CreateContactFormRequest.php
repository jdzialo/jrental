<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateContactFormRequest extends BaseFormRequest {

    public function rules() {

       
        return [
            'firstname' => 'required|regex:/(^[A-Za-z]+$)+/',
            'email' => 'required|email',
            'lastname' => 'required|regex:/(^[A-Za-z]+$)+/',
            'phone' => 'required|regex:/(^[0-9 \+]+$)+/',
            'contactmessage' => 'required',
            'postalcode' => 'required|regex:/^\d\d-\d\d\d$/',
            'country' => 'required|regex:/(^[A-Za-z]+$)+/',
            'address' => 'required',
            'address2' => 'required',
            'city' => 'required|regex:/(^[A-Za-z]+$)+/', 
            'county' => 'required|regex:/(^[A-Za-z]+$)+/'
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'firstname.required' => trans('page::contact.firstname is required'),
            'firstname.regex' => trans('page::contact.firstname is not valid'),
            'lastname.required' => trans('page::contact.lastname is required'),
            'lastname.regex' => trans('page::contact.firstname is not valid'),
            'phone.required' => trans('page::contact.phone is required'),
            'email.required' => trans('page::contact.email is required'),
            'email.email' => trans('page::contact.email is not valid'),
            'phone.regex' => trans('page::contact.phone is not valid'),
            'contactmessage.required' => trans('page::contact.message is required'),
            'postalcode.required' => trans('page::contact.postalcode is required'),
            'postalcode.regex' => trans('page::contact.postalcode is not valid'),
            'country.required' => trans('page::contact.country is required'),
            'country.regex' => trans('page::contact.country is required'),
            'address.required' => trans('page::contact.address is required'),
           
            'address2.required' => trans('page::contact.address2 is required'),
            
            'city.required' => trans('page::contact.city is required'),
            'city.regex' => trans('page::contact.city is not valid'),
            'county.required' => trans('page::contact.county is required'),
            'county.regex' => trans('page::contact.county is not valid'),
        ];
    }

}
